const { FighterRepository } = require('../repositories/fighterRepository')

class FighterService {
	// TODO: Implement methods to work with fighters

	getAll() {
		const fighters = FighterRepository.getAll()
		if (!fighters) {
			return null
		}
		return fighters
	}

	create(newFighter) {
		const fighter = FighterRepository.create(newFighter)
		if (!fighter) {
			return null
		}
		return fighter
	}

	search(search) {
		const item = FighterRepository.getOne(search)
		if (!item) {
			return null
		}
		return item
	}

	delete(id) {
		FighterRepository.delete(id)
		return `${id} deleted`
	}

	update(id, fighter) {
		return FighterRepository.update(id, fighter)
	}
}

module.exports = new FighterService()
