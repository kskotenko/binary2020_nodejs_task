const { UserRepository } = require('../repositories/userRepository')

class UserService {
	// TODO: Implement methods to work with user

	getAll() {
		const users = UserRepository.getAll()
		if (!users) {
			return null
		}
		return users
	}

	create(newUser) {
		const user = UserRepository.create(newUser)
		if (!user) {
			return null
		}
		return user
	}

	search(search) {
		const item = UserRepository.getOne(search)
		if (!item) {
			return null
		}
		return item
	}

	delete(id) {
		UserRepository.delete(id)
		return `${id} deleted`
	}

	update(id, user) {
		return UserRepository.update(id, user)
	}
}

module.exports = new UserService()
