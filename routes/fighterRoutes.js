const { Router } = require('express')
const FighterService = require('../services/fighterService')
const { responseMiddleware } = require('../middlewares/response.middleware')
const {
	createFighterValid,
	updateFighterValid,
} = require('../middlewares/fighter.validation.middleware')

const router = Router()

router
	.route('/')
	.get(function (req, res, next) {
		res.body = FighterService.getAll()
		responseMiddleware(req, res, next)
	})
	.post(
		function (req, res, next) {
			createFighterValid(req, res, next)
		},
		function (req, res, next) {
			const fighter = {
				name: req.body.name,
				power: req.body.power,
				health: req.body.health,
				defense: req.body.defense,
			}

			if (res.statusCode == 200) {
				FighterService.create(fighter)
				res.body = {
					success: 'true',
					message: 'fighter added successfully',
					fighter,
				}
				responseMiddleware(req, res, next)
			}
		}
	)

router
	.route('/:id')
	.get(function (req, res, next) {
		res.body = FighterService.search({ id: req.params.id })
		responseMiddleware(req, res, next)
	})
	.put(
		function (req, res, next) {
			const foundFighter = FighterService.search({ id: req.params.id })
			if (foundFighter == null) {
				res.body = {
					success: 'false',
					message: 'fighter not found',
				}
				responseMiddleware(req, res, next)
			} else {
				updateFighterValid(req, res, next)
			}
			next()
		},
		function (req, res, next) {
			if (res.statusCode == 200) {
				let fighter = {}
				if (req.body.name) {
					fighter.name = req.body.name
				}
				if (req.body.health) {
					fighter.health = req.body.health
				}
				if (req.body.power) {
					fighter.power = req.body.power
				}
				if (req.body.defense) {
					fighter.defense = req.body.defense
				}

				fighter = FighterService.update(req.params.id, fighter)
				res.body = {
					success: 'true',
					message: 'fighter updated successfully',
					fighter,
				}
				responseMiddleware(req, res, next)
			}
		}
	)
	.delete(function (req, res, next) {
		res.body = FighterService.delete(req.params.id)
		responseMiddleware(req, res, next)
	})

// TODO: Implement route controllers for fighter

module.exports = router

// FIGHTER
// GET /api/fighters
// GET /api/fighters/:id
// POST /api/fighters
// PUT /api/fighters/:id
// DELETE /api/fighters/:id
