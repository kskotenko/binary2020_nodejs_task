const { Router } = require('express')
const UserService = require('../services/userService')
const {
	createUserValid,
	updateUserValid,
} = require('../middlewares/user.validation.middleware')
const { responseMiddleware } = require('../middlewares/response.middleware')

const router = Router()

router
	.route('/')
	.get(function (req, res, next) {
		res.body = UserService.getAll()
		responseMiddleware(req, res, next)
	})
	.post(
		function (req, res, next) {
			createUserValid(req, res, next)
		},
		function (req, res, next) {
			const user = {
				firstName: req.body.firstName,
				lastName: req.body.lastName,
				email: req.body.email,
				phoneNumber: req.body.phoneNumber,
				password: req.body.password,
			}

			if (res.statusCode == 200) {
				UserService.create(user)
				res.body = {
					success: 'true',
					message: 'user added successfully',
					user,
				}
				responseMiddleware(req, res, next)
			}
		}
	)

router
	.route('/:id')
	.get(function (req, res, next) {
		res.body = UserService.search({ id: req.params.id })
		responseMiddleware(req, res, next)
	})
	.put(
		function (req, res, next) {
			const foundUser = UserService.search({ id: req.params.id })
			if (foundUser == null) {
				res.body = {
					success: 'false',
					message: 'user not found',
				}
				responseMiddleware(req, res, next)
			} else {
				updateUserValid(req, res, next)
			}
			next()
		},
		function (req, res, next) {
			if (res.statusCode == 200) {
				let user = {}
				if (req.body.firstName) {
					user.firstName = req.body.firstName
				}
				if (req.body.lastName) {
					user.lastName = req.body.lastName
				}
				if (req.body.email) {
					user.email = req.body.email
				}
				if (req.body.phoneNumber) {
					user.phoneNumber = req.body.phoneNumber
				}
				if (req.body.password) {
					user.password = req.body.password
				}

				user = UserService.update(req.params.id, user)
				res.body = {
					success: 'true',
					message: 'user updated successfully',
					user,
				}
				responseMiddleware(req, res, next)
			}
		}
	)
	.delete(function (req, res, next) {
		res.body = UserService.delete(req.params.id)
		responseMiddleware(req, res, next)
	})

module.exports = router
