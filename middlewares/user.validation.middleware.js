const { user } = require('../models/user')

const createUserValid = (req, res, next) => {
	let emailRegEx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@gmail.com$/
	let phoneNumberRegEx = /^[\+]380[0-9]{9}$/

	if (!req.body.firstName) {
		res.status(400).send({
			success: 'false',
			message: 'firstName is required',
		})
	} else if (!req.body.lastName) {
		res.status(400).send({
			success: 'false',
			message: 'lastName is required',
		})
	} else if (!req.body.email) {
		res.status(400).send({
			success: 'false',
			message: 'email is required',
		})
	} else if (req.body.email && !emailRegEx.test(req.body.email)) {
		res.status(400).send({
			success: 'false',
			message: 'email domain must be gmail.com',
		})
	} else if (!req.body.phoneNumber) {
		res.status(400).send({
			success: 'false',
			message: 'phoneNumber is required',
		})
	} else if (
		req.body.phoneNumber &&
		!phoneNumberRegEx.test(req.body.phoneNumber)
	) {
		res.status(400).send({
			success: 'false',
			message: 'phoneNumber must be in the format +380xxxxxxxxx',
		})
	} else if (!req.body.password) {
		res.status(400).send({
			success: 'false',
			message: 'password is required',
		})
	} else if (req.body.password.length < 3) {
		res.status(400).send({
			success: 'false',
			message: 'password must be at least 3 characters',
		})
	} else if (req.body.id) {
		res.status(400).send({
			success: 'false',
			message: 'id must not be provided',
		})
	} else if (typeof req.body === 'object') {
		Object.keys(req.body).forEach(function (key) {
			if (key === 'id') return true
			if (!Object.keys(user).includes(key)) {
				res.status(400).send({
					success: 'false',
					message: `this key ${key} must not be provided`,
				})
			}
		})
	} else {
		res.status(200)
	}

	next()
}

const updateUserValid = (req, res, next) => {
	// TODO: Implement validatior for user entity during update
	let emailRegEx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@gmail.com$/
	let phoneNumberRegEx = /^[\+]380[0-9]{9}$/

	if (req.body.phoneNumber && !phoneNumberRegEx.test(req.body.phoneNumber)) {
		res.status(400).send({
			success: 'false',
			message: 'phoneNumber must be in the format +380xxxxxxxxx',
		})
	} else if (req.body.email && !emailRegEx.test(req.body.email)) {
		res.status(400).send({
			success: 'false',
			message: 'email domain must be gmail.com',
		})
	} else if (req.body.password && req.body.password.length < 3) {
		res.status(400).send({
			success: 'false',
			message: 'password must be at least 3 characters',
		})
	} else if (req.body.id) {
		res.status(400).send({
			success: 'false',
			message: 'id must not be provided',
		})
	} else if (typeof req.body === 'object') {
		Object.keys(req.body).forEach(function (key) {
			if (key === 'id') return true
			if (!Object.keys(user).includes(key)) {
				res.status(400).send({
					success: 'false',
					message: `this key ${key} is not needed`,
				})
			}
		})
	} else {
		res.status(200)
	}
	next()
}

exports.createUserValid = createUserValid
exports.updateUserValid = updateUserValid
