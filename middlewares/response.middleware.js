const responseMiddleware = (req, res, next) => {
	if (
		res.body == null ||
		(res.body.constructor === Array && !res.body.length)
	) {
		return res.status(404).send({
			success: 'false',
			message: 'not found',
		})
	}
	return res.status(201).send(res.body)
}

exports.responseMiddleware = responseMiddleware
