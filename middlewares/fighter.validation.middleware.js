const { fighter } = require('../models/fighter')

const createFighterValid = (req, res, next) => {
	// TODO: Implement validatior for fighter entity during creation

	if (!req.body.name) {
		res.status(400).send({
			success: 'false',
			message: 'name is required',
		})
	} else if (req.body.power && isNaN(req.body.power)) {
		res.status(400).send({
			success: 'false',
			message: 'power must be numeric',
		})
	} else if (req.body.power && req.body.power > 100) {
		res.status(400).send({
			success: 'false',
			message: 'power must be less than 100',
		})
	} else if (req.body.health && isNaN(req.body.health)) {
		res.status(400).send({
			success: 'false',
			message: 'health must be numeric',
		})
	} else if (req.body.health && req.body.health > 100) {
		res.status(400).send({
			success: 'false',
			message: 'health must be less than 100',
		})
	} else if (req.body.defense && isNaN(req.body.defense)) {
		res.status(400).send({
			success: 'false',
			message: 'defense must be numeric',
		})
	} else if (req.body.defense && req.body.defense > 10) {
		res.status(400).send({
			success: 'false',
			message: 'defense must be less than 10',
		})
	} else if (req.body.id) {
		res.status(400).send({
			success: 'false',
			message: 'id must not be provided',
		})
	} else if (typeof req.body === 'object') {
		Object.keys(req.body).forEach(function (key) {
			if (key === 'id') return true
			if (!Object.keys(fighter).includes(key)) {
				res.status(400).send({
					success: 'false',
					message: `this key ${key} is must not be provided`,
				})
			}
		})
	} else {
		res.status(200)
	}

	next()
}

const updateFighterValid = (req, res, next) => {
	// TODO: Implement validatior for fighter entity during update
	if (req.body.power && isNaN(req.body.power)) {
		res.status(400).send({
			success: 'false',
			message: 'power must be numeric',
		})
	} else if (req.body.power && req.body.power > 100) {
		res.status(400).send({
			success: 'false',
			message: 'power must be less than 100',
		})
	} else if (req.body.health && isNaN(req.body.health)) {
		res.status(400).send({
			success: 'false',
			message: 'health must be numeric',
		})
	} else if (req.body.health && req.body.health > 100) {
		res.status(400).send({
			success: 'false',
			message: 'health must be less than 100',
		})
	} else if (req.body.defense && isNaN(req.body.defense)) {
		res.status(400).send({
			success: 'false',
			message: 'defense must be numeric',
		})
	} else if (req.body.defense && req.body.defense > 10) {
		res.status(400).send({
			success: 'false',
			message: 'defense must be less than 10',
		})
	} else if (req.body.id) {
		res.status(400).send({
			success: 'false',
			message: 'id must not be provided',
		})
	} else if (typeof req.body === 'object') {
		Object.keys(req.body).forEach(function (key) {
			if (key === 'id') return true
			if (!Object.keys(fighter).includes(key)) {
				res.status(400).send({
					success: 'false',
					message: `this key ${key} is not needed`,
				})
			}
		})
	} else {
		res.status(200)
	}
	next()
}

exports.createFighterValid = createFighterValid
exports.updateFighterValid = updateFighterValid

// exports.fighter = {
//     "id": "",
//     "name": "",
//     "health": 100,
//     "power": 0,
//     "defense": 1, // 1 to 10
// }
